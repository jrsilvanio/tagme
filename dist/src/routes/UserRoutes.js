"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const UserLocationController_1 = __importDefault(require("../controler/UserLocationController"));
const router = express_1.default.Router();
router
    .get("/user/:id", UserLocationController_1.default.findById)
    .post("/save", UserLocationController_1.default.saveLocation);
exports.default = router;
