"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const locationSchema = new mongoose_1.Schema({
    latitude: { type: String, required: true },
    longitude: { type: String, required: true },
    description: { type: String, required: false },
}, {
    versionKey: false
});
const locationModel = (0, mongoose_1.model)('locationModel', locationSchema);
exports.default = locationModel;
