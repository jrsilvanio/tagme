"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dbConnect_1 = __importDefault(require("./config/dbConnect"));
const index_1 = __importDefault(require("./routes/index"));
dbConnect_1.default.on("error", console.log.bind(console, 'Connection Error'));
dbConnect_1.default.once("open", () => {
    console.log('Conection Sucessful');
});
const app = (0, express_1.default)();
app.use(express_1.default.json());
(0, index_1.default)(app);
exports.default = app;
