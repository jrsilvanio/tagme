"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const IUser_1 = __importDefault(require("../models/IUser"));
class UserLocationController {
}
_a = UserLocationController;
UserLocationController.saveLocation = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let user = new IUser_1.default(req.body);
    let familiesUsers = req.body.familyMembers;
    const families = [];
    if (families) {
        for (const member of familiesUsers) {
            const memberSave = yield new IUser_1.default(member).save();
            families.push(memberSave);
        }
    }
    user.familyMembers = families;
    user.save()
        .then((user) => {
        res.status(201).send(user.toJSON());
    }).catch((err) => {
        res.status(500).send({ message: err.message });
    });
});
UserLocationController.findById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = req.params.id;
    const users = yield IUser_1.default.findById(id).populate('familyMembers').exec();
    res.status(200).send(users);
});
exports.default = UserLocationController;
