"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
mongoose.connect("mongodb://127.0.0.1:27017/tagme");
let db = mongoose.connection;
exports.default = db;
