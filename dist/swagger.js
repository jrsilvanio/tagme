"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const swagger_autogen_1 = __importDefault(require("swagger-autogen"));
// @ts-ignore
const doc = Object.assign({}, );
const outputFile = './swagger-output.json';
const endpointsFiles = [path_1.default.join(__dirname, 'routes/index.ts')];
(0, swagger_autogen_1.default)()(outputFile, endpointsFiles, doc);
