import {Request, Response} from "express";
import UserModel, {IUser} from "../models/IUser";

class UserLocationController {

    static saveLocation = async (req: Request, res: Response) => {
        let user = new UserModel(req.body);
        let familiesUsers = req.body.familyMembers
        const families : IUser[] = []
        if(families){
            for (const member of familiesUsers) {
                 const memberSave = await new UserModel(member).save()
                families.push(memberSave)
            }
        }
        user.familyMembers = families
        user.save()
            .then((user): void => {
                res.status(201).send(user.toJSON())
        }).catch((err) => {
            res.status(500).send({message: err.message})
        })
    }

    static findById =  async(req: Request, res: Response) => {
        const id : String = req.params.id;
        const users = await UserModel.findById(id).populate('familyMembers').exec()
        res.status(200).send(users);
    }

}

export default UserLocationController