import mongoose, {Schema, model, Model} from 'mongoose';

export  interface IUser {
    name: string;
    phone?: string;
    age?: number;
    specialNeeds: boolean;
    descriptionSpecialNeeds?: string;
    familyMembers?: IUser[];
    createdAt?: Date;
}

const userSchema: Schema<IUser, Model<IUser>> = new Schema<IUser>({
        name: {type: String, required: true},
        phone: {type: String, required: false},
        age: {type: Number, required: false},
        specialNeeds: {type: Boolean, required: true},
        descriptionSpecialNeeds: {type: String, required: false},
        familyMembers: [{type: mongoose.Schema.Types.ObjectId, ref: 'userModel', required: false}],
        createdAt: {type: Date, default: Date.now}
    },
    {
        versionKey: false
    });

const UserModel = model<IUser>('userModel', userSchema);
export default UserModel;