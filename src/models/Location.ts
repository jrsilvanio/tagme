import mongoose, {Schema, model, connect, Model, SchemaTypes} from 'mongoose';

interface ILocation {
    latitude: mongoose.Types.Decimal128
    longitude: mongoose.Types.Decimal128
    description?: String;
}

const locationSchema: Schema<ILocation, Model<ILocation>> = new Schema<ILocation>({
        latitude: {type: String, required: true},
        longitude: {type: String, required: true},
        description: {type: String, required: false},
    },
    {
        versionKey: false
    });

const locationModel = model<ILocation>('locationModel', locationSchema);
export default locationModel;