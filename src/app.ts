import express, {Express, Request, Response} from 'express';
import db from "./config/dbConnect";
import routes from "./routes/index";
db.on("error", console.log.bind(console, 'Connection Error'))
db.once("open", () => {
    console.log('Conection Sucessful')
})

const app: Express = express();
app.use(express.json());
routes(app);

export default app
