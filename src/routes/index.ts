import express, {Request, Response} from "express";
import UserRoutes from "./UserRoutes";

const routes = (app: any) => {
    app.route('/').get((req: Request, res: Response) => {
        res.status(200).send({title: "Node"})
    })

    app.use(
        express.json(),
        UserRoutes
    )
}

export default routes