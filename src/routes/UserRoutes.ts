import express, {Router} from "express";
import UserLocationController from "../controler/UserLocationController";

const router : Router = express.Router();

router
    .get("/user/:id", UserLocationController.findById)
    .post("/save", UserLocationController.saveLocation)
export default router;